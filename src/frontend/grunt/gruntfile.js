module.exports = function(grunt) {
	// Grunt initial configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		notify_hooks: {
		    options: {
		      enabled: true,
		      max_jshint_notifications: 5, // maximum number of notifications from jshint output
		      title: "Best For Ages SCSS", // defaults to the name in package.json, or will use project directory's name
		      success: true, // whether successful grunt executions should be notified automatically
		      duration: 10 // the duration of notification in seconds, for `notify-send only
		    },
 	        sass:{
		        options:{
		            title: "CSS Files built",
		            message: "SASS task complete"
		        }
		    }
		},
		// Compress the SASS files.
		sass: {
			options: {
				sourceMap: true,
				outputStyle: 'compressed'
			},
			dist: {
				files: {
					'../css/app.min.css' : 'scss/main.scss'
				}
			}
		},
		uglify: {
			options: {
				preserveComments: 'some',
				mangle: 'false',
				beautify: true
			},
			my_target: {
				files: {
					'../js/app.min.js': [
						'../js/thirdparty/codebird.js', 
						'../js/thirdparty/doT.min.js', 
						'../js/thirdparty/custom-select-min.js', 
						'../js/thirdparty/moment.js', 
						'../js/thirdparty/jquery-scrolltofixed-min.js',
						'../js/thirdparty/social-feeds.js', 
						'../js/thirdparty/tweecool.min.js', 
						'../js/thirdparty/slick.min.js', 
						'../js/thirdparty/jpages.min.js', 
						'../js/bfa.js', 
						'!../js/app.min.js', 
						'!../js/thirdparty/jquery.min.js'
					]
				}
			}
		},
		// Watch files for changes.
		watch: {
			css: {
				files: '**/*.scss',
				tasks: ['sass', 'notify_hooks:sass']
			},
			js: {
				files: ['../js/*.js', '!../js/app.min.js'],
				tasks: ['uglify']
			},
		}
	});
	
	// Load in NPM modules.
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-notify');
	// Register tasks to be ruadfsfn.
	grunt.registerTask('default',['watch']);
}