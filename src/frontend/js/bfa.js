/*
 * General cross site JS (navs, etc)
 */
var General = {
	nav: {},
	ua: {},
	gallery: {}
}

General.nav.init = function() {
	jQuery('.mobile-opener').on('click', function(event) {
		event.preventDefault();
		jQuery('#main-nav .primary').slideToggle();
		jQuery(this).toggleClass('open');
	});

	  	jQuery('#nav').scrollToFixed({
	  		minWidth: '1024'
	  	});
}

// Basic Gallery
General.gallery.init = function() {
  $(".gallery-main").jPages({
      containerID : "thumbs",
      perPage     : 8,
      previous    : ".thumbs-prev",
      next        : ".thumbs-next",
      links       : "blank",
      direction   : "auto",
      animation   : "fadeIn"
  });

  $('.gallery-prev').on('click', function() {
    $(this).closest('.gallery').find('li.selected').prev().trigger('click');
  });

  $('.gallery-next').on('click', function() {
    $(this).closest('.gallery').find('li.selected').next().trigger('click');
  });


  $("ul#thumbs li").click(function(){

    $(this).addClass("selected")
    .siblings()
    .removeClass("selected");

    var img = $(this).children().clone().addClass("animated fadeIn");
    $("div.gallery-main").html( img );

    $(this).closest('.gallery').find('a.zoom').attr('data-img', $(this).children('img').attr('src'));

  });

  $('a.zoom').on('click', function(event) {
      event.preventDefault();

      $('#zoom-modal img').attr('src', $(this).attr('data-img'));

      $.magnificPopup.open({
          items: {
              src: '#zoom-modal'
          },
          mainClass: 'mfp-fade',
          closeBtnInside: true,
          removalDelay: 150
      });
  });

  $("ul#thumbs li:first-child").trigger('click');
}

/*
 * A little user agent detection
 */
 General.ua.init = function() {
    var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);
 }

var Social = {
	twitter: {},
	facebook: {}
}

Social.facebook.init = function() {
	 jQuery('.facebook-block').socialfeed({
	    // FACEBOOK
	    facebook: {
	        accounts: ['@MethodistHomes', ''],
	        limit: 1,
	        access_token: '726546190781095|62b94c24077ec2cf8f6f1f141bddfbcb'
	    },
	    // GENERAL SETTINGS
	    length: 200,
	    show_media: true,
	    // Moderation function - if returns false, template will have class hidden
	    moderation: function(content) {
	        return (content.text) ? content.text.indexOf('') == -1 : true;
	    },
	    //update_period: 5000,
	    // When all the posts are collected and displayed - this function is evoked
	    callback: function() {
	        console.log('Facebook posts retrieved');
	    }
	});
}

Social.twitter.init = function() {
	if(is_ie()) {
	    jQuery('.twitter-block .content .media-body .social-feed-text').tweecool({
	      //settings
	       username : 'MethodistHomes', 
	       limit : 1,
	       profile_image: false
	    });
	} else {
		 jQuery('.twitter-block').socialfeed({
		    // FACEBOOK
			twitter:{
		        accounts: ['@MethodistHomes', ''],                      //Array: Specify a list of accounts from which to pull tweets
		        limit: 1,                                   //Integer: max number of tweets to load
		        consumer_key: 'hemNjWM2w3sibGIAJlmQzyN4C',          //String: consumer key. make sure to have your app read-only
		        consumer_secret: 'c6DdcTcKGrl06XYkxrt9oL7Vm3e2sYwIRDejNuAPbRp2rSND3E',//String: consumer secret key. make sure to have your app read-only
		     },
		    // GENERAL SETTINGS
		    length: 200,
		    show_media: true,
		    // Moderation function - if returns false, template will have class hidden
		    moderation: function(content) {
		        return (content.text) ? content.text.indexOf('') == -1 : true;
		    },
		    //update_period: 5000,
		    // When all the posts are collected and displayed - this function is evoked
		    callback: function() {
		        console.log('Twitter posts retrieved');
		    }
		});
	}
}

var Slick = {
	mini_slick: {},
	wide_slick: {},
	event_slick: {},
	news_slick: {}
}

Slick.mini_slick.init = function() {
	jQuery('.slick-mini').slick({
	    arrows: false,
	    autoplay: true,
	    autoplaySpeed: 2000,
	    dots: true, 
	    infinite: true,
	    speed: 500,
	    responsive: true,
	    slide: '.slide',
	    slidesToShow: 1,
	    slidesToScroll: 1
	});
}

Slick.wide_slick.init = function() {
	jQuery('.slick-wide').slick({
	    arrows: false,
	    autoplay: false,
	    autoplaySpeed: 2000,
	    dots: true, 
	    infinite: true,
	    speed: 500,
	    responsive: true,
	    slide: '.slide',
	    slidesToShow: 1,
	    slidesToScroll: 1
	});
}

Slick.event_slick.init = function() {
	jQuery('.slick-events').slick({
	    arrows: true,
	    autoplay: false,
	    dots: false, 
	    infinite: true,
	    speed: 500,
	    cssEase: 'ease-in-out',
	    responsive: [
		    {
		      breakpoint: 979,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true,
		        arrows: false
		      }
		    },
	    	{
		      breakpoint: 740,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true,
		        arrows: false
		      }
		    }
		],
	    slide: '.slide',
	    slidesToShow: 2,
	    slidesToScroll: 1,
	    prevArrow:"<span class='a-left control-c prev slick-prev'></span>",
	    nextArrow:"<span class='a-right control-c next slick-next'></span>"
	});
}

Slick.news_slick.init = function() {
	jQuery('.slick-news').slick({
	    arrows: true,
	    autoplay: false,
	    dots: false, 
	    infinite: true,
	    speed: 500,
	    cssEase: 'ease-in-out',
	    responsive: [
		    {
		      breakpoint: 979,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true,
		        arrows: false
		      }
		    },
	    	{
		      breakpoint: 740,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true,
		        arrows: false
		      }
		    }
		],
	    slide: '.slide',
	    slidesToShow: 3,
	    slidesToScroll: 1,
	    prevArrow:"<span class='a-left control-c prev slick-prev'></span>",
	    nextArrow:"<span class='a-right control-c next slick-next'></span>"
	});
}



/*
 * Returns true if browser is IE 8 up to 10
 */
function is_ie() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    if (msie > 0) {
        return true;
    } else {
        return false;
    }
}